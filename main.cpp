#include <iostream>
#include <vector>
#include <cstring>

void decode(int argc, char **);

int main(int charv, char *argv[]){
	if (charv == 1){
		std::cout << "No arguments entered" << std::endl;
	}else if (charv >=1){
		decode(charv, argv);		
	}
}


void decode(int argc, char ** argv){
	argc--;
	int sizes[argc];
	std::vector<std::string> args;
	for (int i = 0; i < argc; i++){
		args.push_back(argv[i + 1]);
		sizes[i] = args[i].length();
	}
	int alpha = 26;
	for (int i = 0; i < alpha; i++){
		for (int j = 0; j < argc; j++){
			for (int k = 0; k < sizes[j]; k++){
				args[j][k]++;
				if (args[j][k] > 122){
					args[j][k] = 97;
				}
			}
			std::cout << args[j] << " ";
		}
		std::cout << std::endl;
	}
}
